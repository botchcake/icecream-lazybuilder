<?php

namespace Drupal\icecream;

class Icecream {

  /**
   * Returns a renderable array containing an ice cream flavour.
   *
   * @return array
   */
  public function lazyCallback() {
    $flavors = [
      'chocolate',
      'vanilla',
      'chocolate chip cookie dough',
      'grape',
      'straciatella',
      'rocky road',
    ];

    // This is never cacheable, because of the max_age 0.
    return [
      '#markup' => $flavors[array_rand($flavors)],
      '#cache' => ['max_age' => 0],
    ];
  }
}
