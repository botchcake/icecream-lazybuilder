<?php

namespace Drupal\icecream\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Session\AccountInterface;

/**
 * Provides a message showing the Icecream status, with some uncacheable info.
 *
 * @Block(
 *   id = "icecream_lazy_block",
 *   admin_label = @Translation("Icecream Lazy"),
 * )
 */
class IcecreamLazyBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $build = [];

    // This placeholder is just a unique string, it has no other requirements
    // other than being injected in the places than where you want to have the
    // #lazy_builder take over.
    $placeholder = 'iceCream' . crc32('lazy icecream block');

    // Makes sure the placeholder will be replaced by an ice cream flavor by
    // attaching a lazy builder that will make the rest of this block cacheable.
    $build['#attached']['placeholders'][$placeholder] = [
      '#lazy_builder' => ['icecream:lazyCallback', [$placeholder]]
    ];

    // This is really intensive to calculate, so that's why we're caching the
    // entire block and having the lazy builder take care of the uncacheable
    // part of the block.
    $build['#markup'] = 'This is being generated with a placeholder.
    It is time for ' . $placeholder . ' ice cream. Hurray for ice cream!';


    // Returns the renderable array with attached placeholder.
    return $build;
  }

}
